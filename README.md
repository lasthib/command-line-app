Here is a simple command line app using spring-boot.

## requirements

Java 8, Maven.


## run

- Compile, test and package: ```mvn package```
- Run: ```java -jar target/command-line-app-1.jar ./input.txt```

