package com.example.commandlineapp;

/**
 * An Input represent an order that the drone has to deliver.
 */
public class Input {

  private String id;
  private int latitude;
  private int longitude;
  private String timestamp; // when the order was made
  private String delivery; // when the drone makes the delivery

  /**
   * constructor to do the text parsing from the text file
   * 
   * @param line
   */
  public Input(String line) {
    String[] lineTab = line.split(" ");
    this.id = lineTab[0];
    String c = lineTab[1];
    String lat = c.split("\\s*[EW]+\\s*")[0];
    String lng = c.split("[NS]+[0-9]+\\s*")[1];
    this.latitude = Integer.parseInt(lat.substring(1));
    if (lat.charAt(0) == 'S') {
      this.latitude = -this.latitude;
    }

    this.longitude = Integer.parseInt(lng.substring(1));
    if (lng.charAt(0) == 'W') {
      this.longitude = -this.longitude;
    }

    this.timestamp = lineTab[2];
  }

  public Input() {
  }

  /**
   * get the distance to the warehouse
   * 
   * @return number of blocks to the warehouse
   */
  public int getDistanceToWarehouse() {
    return Math.abs(this.latitude) + Math.abs(this.longitude);
  }

  public String toString() {
    if (delivery != null) {
      return id + " " + delivery;
    } else {
      return id;
    }
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getLatitude() {
    return latitude;
  }

  public void setLatitude(int latitude) {
    this.latitude = latitude;
  }

  public int getLongitude() {
    return longitude;
  }

  public void setLongitude(int longitude) {
    this.longitude = longitude;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getDelivery() {
    return delivery;
  }

  public void setDelivery(String delivery) {
    this.delivery = delivery;
  }
}
